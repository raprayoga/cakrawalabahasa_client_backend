<?php

namespace App\Models;

use App\Models\ParentModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersEmailVerifiedRequest extends ParentModel
{
    use SoftDeletes;

    protected $table = 'users_email_verified_request';
    protected $softdelete;
    protected $hidden = ['updated_at', 'deleted_at'];
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
