<?php

namespace App\Models;

use App\Models\ParentModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class DokumentasiBatch extends ParentModel
{
    use SoftDeletes;
    
    protected $table = 'dokumentasi_batch';
    protected $hidden = ['updated_at', 'deleted_at'];

    public function dokumentasi()
    {
        return $this->hasMany(Dokumentasi::class, 'batch_id');
    }

    public function scopeSearch($query, $searchText = null)
    {
        if ($searchText !== '*' && $searchText !== null) {
            $query->where('judul', 'LIKE', '%' . $searchText . '%');
        }
        
        return $query;
    }
}
