<?php

namespace App\Models;

use App\Models\ParentModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistIntHeroes extends ParentModel
{
  use SoftDeletes;

  protected $table = 'regist_int_heroes';
  protected $softdelete;
  protected $hidden = ['updated_at', 'deleted_at'];
  protected $fillable = ['full_name', 'wa_number', 'nationality', 'age', 'status', 'language_speake', 'language_teach', 'division', 'follow_ig'];
  protected $guarded = ['id'];
}
