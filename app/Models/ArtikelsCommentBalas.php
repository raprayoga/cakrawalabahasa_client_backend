<?php

namespace App\Models;

use App\Models\ParentModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArtikelsCommentBalas extends ParentModel
{
    use SoftDeletes;

    protected $table = 'artikels_comment_balas';
    protected $hidden = ['updated_at', 'deleted_at'];
    protected $fillable = ['comment_id', 'user_id', 'comment', 'like'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ArtikelsComment()
    {
        return $this->belongsTo(ArtikelsComment::class, 'comment_id');
    }

    public function likeCommentBalas()
    {
        return $this->hasMany(LikeCommentBalas::class);
    }
}
