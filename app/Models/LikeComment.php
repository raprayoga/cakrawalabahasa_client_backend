<?php

namespace App\Models;

use App\Models\ParentModel;

class LikeComment extends ParentModel
{
    protected $table = 'like_comment';
    protected $guard = [];
    protected $primaryKey = 'artikels_comment_id';
    public $timestamps = false;

    public function artikelsComment()
    {
        return $this->belongsTo(ArtikelsComment::class);
    }
}
