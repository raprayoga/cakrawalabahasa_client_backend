<?php

namespace App\Models;

use App\Models\ParentModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistMember extends ParentModel
{
  use SoftDeletes;
  
  protected $table = 'regist_member';
  protected $softdelete;
  protected $hidden = ['updated_at', 'deleted_at'];
  protected $fillable = ['nama_lengkap', 'nomor_wa', 'tgl_lahir', 'alamat', 'kota', 'status', 'instansi', 'minat', 'paket', 'follow_ig', 'active'];
  protected $guarded = ['id'];
}
