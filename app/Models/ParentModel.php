<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Contracts\Activity;

class ParentModel extends Model
{
    use LogsActivity; 

    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $logAttributesToIgnore = [ 'updated_at'];
    public $data = [];

    public function setTapActivity($data = [])
    {
        $this->data = $data;
    }

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->ip_address = $_SERVER["REMOTE_ADDR"];
        $log_name = $description = $properties = $causer_id = $causer_type = $subject_id = $subject_type = NULL;
        extract($this->data);

        if ($description) $activity->description = $eventName . ' : ' . $description;
        if ($log_name) $activity->log_name = $log_name;
        if ($properties) $activity->properties = $properties;
        if ($causer_id) $activity->causer_id = $causer_id;
        if ($causer_type) $activity->causer_type = $causer_type;
        if ($subject_id) $activity->subject_id = $subject_id;
        if ($subject_type) $activity->subject_type = $subject_type;
    }
}
