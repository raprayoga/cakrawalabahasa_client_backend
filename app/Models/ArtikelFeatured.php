<?php

namespace App\Models;

use App\Models\ParentModel;

class ArtikelFeatured extends ParentModel
{
    protected $table = 'artikel_featured';
    protected $hidden = ['created_at', 'updated_at'];

    public function artikel()
    {
        return $this->belongsTo(Artikel::class, 'artikel_id', 'id');
    }
}
