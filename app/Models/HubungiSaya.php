<?php

namespace App\Models;

use App\Models\ParentModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class HubungiSaya extends ParentModel
{
    use SoftDeletes;

    protected $table = 'hubungi_kami';
    protected $fillable = ['nama', 'email', 'phone', 'metode', 'pesan', 'read_at'];
}
