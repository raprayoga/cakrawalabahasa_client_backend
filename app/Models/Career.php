<?php

namespace App\Models;

use App\Models\ParentModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Career extends ParentModel
{
    use SoftDeletes;

    protected $table = 'career';
    protected $hidden = ['updated_at', 'deleted_at'];
    protected $fillable = ['email', 'full_name', 'nick_name', 'gender', 'age', 'domicile', 'phone_number', 'institute', 'major', 'semester', 'intern_info', 'position', 'why_match'];
    protected $guard = ['id'];

    protected static function booted()
    {
        // default setiap query, bila tidak ingin digunakan ->withoutGlobalScopes(['order_desc'])->get();
        static::addGlobalScope('order_desc', function (Builder $builder) {
            $builder->orderBy('id', 'desc');
        });
    }
}
