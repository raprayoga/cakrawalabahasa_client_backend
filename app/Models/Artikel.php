<?php

namespace App\Models;

use App\Models\ParentModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Artikel extends ParentModel
{
    use SoftDeletes;
    
    protected $table = 'artikel';

    public function artikelFeatured()
    {
        return $this->hasMany(ArtikelFeatured::class, 'artikel_id');
    }

    public function artikelKategori()
    {
        return $this->belongsTo(ArtikelKategori::class, 'kategori_id', 'id');
    }

    protected static function booted()
    {
        // default setiap query, bila tidak ingin digunakan ->withoutGlobalScopes(['order_desc'])->get();
        static::addGlobalScope('order_desc', function (Builder $builder) {
            $builder->orderBy('id', 'desc');
        });
    }

    public function scopekategori($query, $kategori = null)
    {
        if ($kategori) {
            $query->where('kategori_id', $kategori);
        }
        
        return $query;
    }

    public function scopeSearch($query, $searchText = null)
    {
        if ($searchText !== '*' && $searchText !== null) {
            $query->where('judul', 'LIKE', '%' . $searchText . '%');
        }
        
        return $query;
    }
}
