<?php

namespace App\Models;

use App\Models\ParentModel;

class LikeCommentBalas extends ParentModel
{
    protected $table = 'like_comment_balas';
    protected $guard = [];
    protected $primaryKey = 'artikels_comment_balas_id';
    public $timestamps = false;

    public function artikelsCommentBalas()
    {
        return $this->belongsTo(ArtikelsCommentBalas::class);
    }
}
