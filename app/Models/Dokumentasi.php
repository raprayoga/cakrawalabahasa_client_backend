<?php

namespace App\Models;

use App\Models\ParentModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dokumentasi extends ParentModel
{
    use SoftDeletes;

    protected $table = 'dokumentasi';
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function batch()
    {
        return $this->belongsTo(DokumentasiBatch::class);
    }
}
