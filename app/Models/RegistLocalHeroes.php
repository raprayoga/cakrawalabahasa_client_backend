<?php

namespace App\Models;

use App\Models\ParentModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistLocalHeroes extends ParentModel
{
  use SoftDeletes;

  protected $table = 'regist_local_heroes';
  protected $softdelete;
  protected $hidden = ['updated_at', 'deleted_at'];
  protected $fillable = ['nama_lengkap', 'nomor_wa', 'domisili', 'usia', 'status', 'instansi', 'divisi1', 'divisi2', 'link_portfolio', 'cv', 'follow_ig'];
  protected $guarded = ['id'];
}
