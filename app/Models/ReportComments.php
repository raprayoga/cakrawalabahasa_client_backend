<?php

namespace App\Models;

use App\Models\ParentModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportComments extends ParentModel
{
    use SoftDeletes;

    protected $table = 'report_comments';
    protected $softdelete;
    protected $hidden = ['updated_at', 'deleted_at'];
    protected $guarded = ['id'];
}
