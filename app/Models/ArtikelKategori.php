<?php

namespace App\Models;

use App\Models\ParentModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArtikelKategori extends ParentModel
{
    use SoftDeletes;
    
    protected $table = 'artikel_kategori';
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function artikel()
    {
        return $this->hasMany(Artikel::class, "kategori_id");
    }
}
