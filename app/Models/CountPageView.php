<?php

namespace App\Models;

use App\Models\ParentModel;

class CountPageView extends ParentModel
{
    protected $table = 'count_page_view';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['page', 'ip_address', 'date'];
}
