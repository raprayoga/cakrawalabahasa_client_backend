<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;
use Image;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $profile)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $profile)
    {
        return response(['profile' => $profile], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $profile)
    {
        $request->validate([
            'name' => 'required|string',
            'username' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|string',
            'alamat' => 'nullable|string',
            'tgl_lahir' => 'nullable|string',
            'tempat_lahir' => 'nullable|string',
            'pendidikan' => 'nullable|string',
            'image' => 'string',
            'image_name' => 'string'
        ]);

        extract($request->all());
        $profile->name = $name;
        $profile->username = $username;
        $profile->email = $email;
        $profile->phone = $phone;
        $profile->alamat = $alamat;
        $profile->tgl_lahir = $tgl_lahir;
        $profile->tempat_lahir = $tempat_lahir;
        $profile->pendidikan = $pendidikan;

        if ($request->image) {
            $tujuan_upload = public_path() . '/img/profile/';
            $image = base64_decode($request->image);

            $img = Image::make($image);
            $img->save($tujuan_upload . $request->image_name, 60);
            $profile->photo_profile = $request->image_name; 
        }

        $profile->setTapActivity(['description' => 'Successfully to Update Profile']); //kirim activitylog desc
        $profile->save();

        $message = 'Profile berhasil diperbarui';
        return response()->json(compact('message', 'profile'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $profile)
    {
        //
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'old_password' => 'required|password:api',
            'new_password' => 'required|min:6|regex:/^.*(?=.{6,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x]).*$/|not_regex:/ /i|confirmed',
            'new_password_confirmation' => 'required|min:6'
        ]);


        $new_password = $request->new_password;
        $patern_cekpassberurutan = "/abcdefghijklmnopqrstuvwxyz01234567890/i";
        if (preg_match($patern_cekpassberurutan, $new_password)) {
            return response()->json(['message' => 'Password karakter huruf/angka tidak boleh berurutan'], 422);
        }

        activity()
            ->performedOn(new User)
            ->tap(function(Activity $activity) {
                $activity->ip_address = $_SERVER["REMOTE_ADDR"];
            })->log(Auth::user()->username . 'Succesfull to reset password');
        $request->user()->fill([
            'password' => Hash::make($new_password)
        ])->save();

        return response()->json(['message' => 'Password berhasil diubah'], 200);
    }
}
