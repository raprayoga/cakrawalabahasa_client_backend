<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\DokumentasiBatch;
use Illuminate\Http\Request;

class DokumentasiBatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dokuemntasi_batchs = DokumentasiBatch::with(array('dokumentasi' => function($query) {
            $query->select('id', 'batch_id', 'image', 'caption', 'created_at');
        }))->get();
        return response(['dokuemntasi_batchs' => $dokuemntasi_batchs], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DokumentasiBatch  $dokumentasi
     * @return \Illuminate\Http\Response
     */
    public function show(DokumentasiBatch $dokumentasi)
    {
        $dokumentasi->load(array('dokumentasi' => function($query) {
            $query->select('id', 'batch_id', 'image', 'caption', 'created_at');
        }));
        return response(['dokumentasi_batch' => $dokumentasi], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DokumentasiBatch  $dokumentasiBatch
     * @return \Illuminate\Http\Response
     */
    public function edit(DokumentasiBatch $dokumentasiBatch)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DokumentasiBatch  $dokumentasiBatch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DokumentasiBatch $dokumentasiBatch)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DokumentasiBatch  $dokumentasiBatch
     * @return \Illuminate\Http\Response
     */
    public function destroy(DokumentasiBatch $dokumentasiBatch)
    {
        //
    }

    public function list(Request $request)
    {
        $request->validate([
            'searchText' => 'string|nullable',
            'per_page' => 'required|numeric',
            'current_page' => 'required|numeric'
        ]);
        $searchText = $kategori = $per_page = $current_page =  NULL;
        extract($request->all());
        $dokumentasiModel = DokumentasiBatch::search($searchText)
        ->orderBy('id', 'desc');
        $total = $dokumentasiModel->count();
        $total_page = ceil($total / $per_page);
        $offset = ($current_page - 1) * $per_page;
        $dokumentasis = $dokumentasiModel->offset($offset)->limit($per_page)->get();
        $dokumentasis->each(function($feed) {
            $feed->load(array('dokumentasi' => function($query) {
                $query->select('id', 'batch_id', 'image')->limit(1);
            }));
        });

        return response()->json(compact(
            'per_page',
            'current_page',
            'total',
            'dokumentasis',
        ), 200);
    }
}
