<?php

namespace App\Http\Controllers\Api\Registration;

use App\Http\Controllers\Controller;
use App\Models\RegistMember;
use Illuminate\Http\Request;

class RegistMemberController extends Controller
{
    private const VALIDATE = [
        'nama_lengkap' => 'required|string|unique:regist_member,nama_lengkap',
        'nomor_wa' => 'required|numeric',
        'tgl_lahir' => 'required|string',
        'alamat' => 'required|string',
        'kota' => 'required|string',
        'status' => 'required|string',
        'instansi' => 'required|string',
        'minat' => 'required|string',
        'paket' => 'required|string',
        'follow_ig' => 'required|string'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(self::VALIDATE);
        $regist = new RegistMember(request()->all());
        $regist->setTapActivity(['description' => 'Successfully to Regist Member. nam: ' . $request->nama_lengkap]); //kirim activitylog desc
        $regist->save();

        return response()->json(['message' => 'Form Anda berhasil diterima'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RegistMember  $registMember
     * @return \Illuminate\Http\Response
     */
    public function show(RegistMember $registMember)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RegistMember  $registMember
     * @return \Illuminate\Http\Response
     */
    public function edit(RegistMember $registMember)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RegistMember  $registMember
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegistMember $registMember)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RegistMember  $registMember
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegistMember $registMember)
    {
        //
    }
}
