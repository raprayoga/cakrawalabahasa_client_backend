<?php

namespace App\Http\Controllers\Api\Registration;

use App\Http\Controllers\Controller;
use App\Models\RegistLocalHeroes;
use Illuminate\Http\Request;

class RegistLocalHeroesController extends Controller
{
    private const VALIDATE = [
        'nama_lengkap' => 'required|string|unique:regist_local_heroes,nama_lengkap',
        'nomor_wa' => 'required|numeric',
        'domisili' => 'required|string',
        'usia' => 'required|numeric',
        'status' => 'required|string',
        'instansi' => 'required|string',
        'divisi1' => 'required|string',
        'divisi2' => 'required|string',
        'link_portfolio' => 'nullable|string',
        'cv' => 'required|string',
        'follow_ig' => 'required|string'
      ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(self::VALIDATE);
        $regist = new RegistLocalHeroes(request()->all());
        $regist->setTapActivity(['description' => 'Successfully to Regist Local Heroes. nam: ' . $request->nama_lengkap]); //kirim activitylog desc
        $regist->save();

        return response()->json(['message' => 'Form Anda berhasil diterima'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RegistLocalHeroes  $registLocalHeroes
     * @return \Illuminate\Http\Response
     */
    public function show(RegistLocalHeroes $registLocalHeroes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RegistLocalHeroes  $registLocalHeroes
     * @return \Illuminate\Http\Response
     */
    public function edit(RegistLocalHeroes $registLocalHeroes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RegistLocalHeroes  $registLocalHeroes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegistLocalHeroes $registLocalHeroes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RegistLocalHeroes  $registLocalHeroes
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegistLocalHeroes $registLocalHeroes)
    {
        //
    }
}
