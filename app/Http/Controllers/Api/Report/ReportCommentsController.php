<?php

namespace App\Http\Controllers\Api\Report;

use App\Http\Controllers\Controller;
use App\Models\ReportComments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportCommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'comment_id' => 'required|numeric',
            'message' => 'max:255|string'
        ]);

        $user_id = Auth::user()->id;

        $report = new ReportComments([
            'artikels_comment_id' => $request->comment_id,
            'artikels_comment_balas_id' => NULL,
            'users_id' => $user_id,
            'message' => $request->message,
        ]);
        $report->setTapActivity(['description' => Auth::user()->username . ': Successfully to Report comment artikel. messge: ' . $request->message]); //kirim activitylog desc
        $report->save();
        
        $message = 'Your report has been send';
        return response(compact('message'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReportComments  $reportComments
     * @return \Illuminate\Http\Response
     */
    public function show(ReportComments $reportComments)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReportComments  $reportComments
     * @return \Illuminate\Http\Response
     */
    public function edit(ReportComments $reportComments)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReportComments  $reportComments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReportComments $reportComments)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReportComments  $reportComments
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportComments $reportComments)
    {
        //
    }
}
