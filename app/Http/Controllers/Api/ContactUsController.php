<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\HubungiSaya;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'pesan' => 'required|string',
            'metode' => 'required|string',
        ]);

        $hubungi_saya = new HubungiSaya($request->all());
        $hubungi_saya->setTapActivity(['description' => 'Successfully to Send Contact Kami']); //kirim activitylog desc
        $hubungi_saya->save();

        return response()->json(['message' => 'Pesan Anda berhasil terkirim'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HubungiSaya  $hubungiSaya
     * @return \Illuminate\Http\Response
     */
    public function show(HubungiSaya $hubungiSaya)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HubungiSaya  $hubungiSaya
     * @return \Illuminate\Http\Response
     */
    public function edit(HubungiSaya $hubungiSaya)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HubungiSaya  $hubungiSaya
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HubungiSaya $hubungiSaya)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HubungiSaya  $hubungiSaya
     * @return \Illuminate\Http\Response
     */
    public function destroy(HubungiSaya $hubungiSaya)
    {
        //
    }

    public function sendNotifHubungiSaya()
    {
        $before_notif = HubungiSaya::whereNull('read_at')->count();
        if ($before_notif < 1) return;

        $email_list = [
            [
                'email' => 'prayogarafie@gmail.com', 
                'username' => 'Yoga'
            ],
            [
                'email' => 'rizqy.hakim07@gmail.com',
                'username' => 'Rizqy'
            ],
            [
                'email' => 'windyagustinkusumaningrum@gmail.com',
                'username' => 'Windy'
            ]
        ];
        foreach ($email_list as $email) {
            $email['count'] = $before_notif;
            Mail::to($email['email'])->send(new \App\Mail\PushNotifHubungikami($email));
        }

        return response()->json(['message' => 'Notifikasi Pesan Anda berhasil terkirim'], 200);
    }
}
