<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CountPageView;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function addPageView()
    {
        $date_now = Carbon::now()->format('Y-m-d');

        CountPageView::create([
            'page' => 'landing',
            'date' => $date_now
        ]);
    }
}
