<?php

namespace App\Http\Controllers\Api;

use App\Models\Career;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CareerController extends Controller
{
    public function apply(Request $request, $position)
    {
        $request->validate([
            'email' => 'required|email',
            'full_name' => 'required|string',
            'nick_name' => 'required|string',
            'gender' => 'required|string',
            'age' => 'required|numeric',
            'domicile' => 'required|string',
            'phone_number' => 'required|string',
            'institute' => 'required|string',
            'major' => 'required|string',
            'semester' => 'required|numeric',
            'intern_info' => 'required|string',
            'why_match' => 'required|string',
        ]);

        $request['position'] = $position;
        // return $request;
        $hubungi_saya = new Career($request->all());
        $hubungi_saya->setTapActivity(['description' => 'Successfully to Apply Lowongan']); //kirim activitylog desc
        $hubungi_saya->save();

        return response()->json(['message' => 'Form Anda berhasil terkirim'], 200);
    }
}
