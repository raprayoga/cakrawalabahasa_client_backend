<?php

namespace App\Http\Controllers\Api\Artikel;

use App\Http\Controllers\Controller;
use App\Models\ArtikelsComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArtikelsCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'comment' => 'required|min:10|string',
            'id' => 'required|numeric'
        ]);

        $user_id = Auth::user()->id;

        $comment = new ArtikelsComment([
            'artikel_id' => $request->id,
            'user_id' => $user_id,
            'comment' => $request->comment,
            'like' => 0
        ]);
        $comment->setTapActivity(['description' => 'Successfully to Post comment, message: ' . $request->comment]); //kirim activitylog desc
        $comment->save();

        $comment->load('user', 'balas.user', 'likeComment', 'balas.likeCommentBalas');
        $comment->like = 0;
        $message = 'Your comment has been post';

        return response(compact('message', 'comment'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArtikelsComment  $artikelsComment
     * @return \Illuminate\Http\Response
     */
    public function show(ArtikelsComment $artikelsComment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArtikelsComment  $artikelsComment
     * @return \Illuminate\Http\Response
     */
    public function edit(ArtikelsComment $artikelsComment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ArtikelsComment  $artikelsComment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArtikelsComment $artikelsComment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArtikelsComment  $artikelsComment
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArtikelsComment $artikelsComment)
    {
        //
    }

    public function commentWithIdArtikel($id)
    {
        $comments = ArtikelsComment::where('artikel_id', $id)
            ->with(array('user' => function($query) {
                $query->select('id', 'username', 'photo_profile');
                }, 'balas.user' => function($query) {
                $query->select('id', 'username', 'photo_profile');
                }))
            ->get();

       return response(['comments' => $comments], 200);
    }

    public function commentWithIdArtikelLogin($id)
    {
        $user_id = Auth::user()->id;
        $comments = ArtikelsComment::where('artikel_id', $id)
            ->with(array('likeComment' => function($query) use ($user_id) {
                $query->where('user_id', $user_id);
                }, 'user' => function($query) {
                $query->select('id', 'username', 'photo_profile');
                }, 'balas.user' => function($query) {
                $query->select('id', 'username', 'photo_profile');
                }, 'balas.likeCommentBalas' => function($query) use ($user_id) {
                $query->where('user_id', $user_id);
            }))
            ->get();

       return response(['comments' => $comments], 200);
    }
}
