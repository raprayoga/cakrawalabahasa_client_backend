<?php

namespace App\Http\Controllers\Api\Artikel;

use App\Http\Controllers\Controller;
use App\Models\Artikel;
use Illuminate\Http\Request;

class ArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artikels = Artikel::take(4)->select('id', 'judul', 'image', 'text_lead', 'created_at')->get();
        return response(['artikels' => $artikels], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function show(Artikel $artikel)
    {
        $read_count = $artikel->read_count;
        $artikel->read_count = $read_count + 1;
        $artikel->save();
        $artikel->load('artikelKategori');
        return response(['artikel' => $artikel], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function edit(Artikel $artikel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Artikel $artikel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Artikel $artikel)
    {
        //
    }

    
    public function list(Request $request)
    {
        $request->validate([
            'searchText' => 'string|nullable',
            'kategori' => 'string|nullable',
            'per_page' => 'required|numeric',
            'current_page' => 'required|numeric'
        ]);
        $searchText = $kategori = $per_page = $current_page =  NULL;
        extract($request->all());
        $artikelModel = Artikel::kategori($kategori)
        ->search($searchText)
        ->select('id', 'kategori_id', 'author', 'judul', 'image', 'text_lead', 'created_at')
        ->orderBy('id', 'desc');
        $total = $artikelModel->count();
        $total_page = ceil($total / $per_page);
        $offset = ($current_page - 1) * $per_page;
        $artikels = $artikelModel->offset($offset)->limit($per_page)->get();

        return response()->json(compact(
            'per_page',
            'current_page',
            'total',
            'artikels',
        ), 200);
    }

    public function populer()
    {
        $artikels = Artikel::take(10)->orderBy('read_count', 'desc')->select('id', 'judul', 'created_at')->get();
        return response()->json(['artikels' => $artikels], 200);
    }
}
