<?php

namespace App\Http\Controllers\Api\Artikel;

use App\Http\Controllers\Controller;
use App\Models\ArtikelsComment;
use App\Models\ArtikelsCommentBalas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArtikelsCommentBalasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'comment' => 'required|min:10|string',
            'id' => 'required|numeric'
        ]);

        $user_id = Auth::user()->id;

        $balas_comment = new ArtikelsCommentBalas([
            'comment_id' => $request->id,
            'user_id' => $user_id,
            'comment' => $request->comment,
            'like' => 0
        ]);
        $balas_comment->setTapActivity(['description' => 'Successfully to Post comment balas, message: ' . $request->comment]); //kirim activitylog desc
        $balas_comment->save();

        $comment = ArtikelsComment::with('user', 'balas.user', 'likeComment', 'balas.likeCommentBalas')->find($request->id);
        $message = 'Your comment has been post';

        return response(compact('message', 'comment'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArtikelsCommentBalas  $artikelsCommentBalas
     * @return \Illuminate\Http\Response
     */
    public function show(ArtikelsCommentBalas $artikelsCommentBalas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArtikelsCommentBalas  $artikelsCommentBalas
     * @return \Illuminate\Http\Response
     */
    public function edit(ArtikelsCommentBalas $artikelsCommentBalas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ArtikelsCommentBalas  $artikelsCommentBalas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArtikelsCommentBalas $artikelsCommentBalas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArtikelsCommentBalas  $artikelsCommentBalas
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArtikelsCommentBalas $artikelsCommentBalas)
    {
        //
    }
}
