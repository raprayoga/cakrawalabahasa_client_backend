<?php

namespace App\Http\Controllers\Api\Artikel;

use App\Http\Controllers\Controller;
use App\Models\ArtikelFeatured;
use Illuminate\Http\Request;

class ArtikelFeaturedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artikel_featureds = ArtikelFeatured::with(array('artikel' => function($query) {
            $query->select('id','kategori_id', 'judul', 'image', 'text_lead', 'created_at');
        }))->get();
        return response(['artikel_featureds' => $artikel_featureds], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArtikelFeatured  $artikelFeatured
     * @return \Illuminate\Http\Response
     */
    public function show(ArtikelFeatured $artikelFeatured)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArtikelFeatured  $artikelFeatured
     * @return \Illuminate\Http\Response
     */
    public function edit(ArtikelFeatured $artikelFeatured)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ArtikelFeatured  $artikelFeatured
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArtikelFeatured $artikelFeatured)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArtikelFeatured  $artikelFeatured
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArtikelFeatured $artikelFeatured)
    {
        //
    }
}
