<?php

namespace App\Http\Controllers\Api\Artikel;

use App\Http\Controllers\Controller;
use App\Models\ArtikelKategori;
use Illuminate\Http\Request;

class ArtikelKategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artikel_kategori = ArtikelKategori::all();
        return response(['artikel_kategori' => $artikel_kategori], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArtikelKategori  $artikelKategori
     * @return \Illuminate\Http\Response
     */
    public function show(ArtikelKategori $artikelKategori)
    {
        return response(['artikelKategori' => $artikelKategori], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArtikelKategori  $artikelKategori
     * @return \Illuminate\Http\Response
     */
    public function edit(ArtikelKategori $artikelKategori)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ArtikelKategori  $artikelKategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArtikelKategori $artikelKategori)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArtikelKategori  $artikelKategori
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArtikelKategori $artikelKategori)
    {
        //
    }
}
