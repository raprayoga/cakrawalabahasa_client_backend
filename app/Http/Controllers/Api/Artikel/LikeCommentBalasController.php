<?php

namespace App\Http\Controllers\Api\Artikel;

use App\Http\Controllers\Controller;
use App\Models\LikeCommentBalas;
use App\Models\ArtikelsCommentBalas;
use App\Models\ArtikelsComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeCommentBalasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->id;
        $comment_balas_id = $request->id;
        $comment = ArtikelsCommentBalas::find($comment_balas_id);

        $like_comment_balas = LikeCommentBalas::where('artikels_comment_balas_id', $comment_balas_id)
        ->where('user_id', $user_id)
        ->first();

        if ($like_comment_balas) {
            $comment->like = $comment->like == 0 ? 0 : $comment->like - 1;
            LikeCommentBalas::where('artikels_comment_balas_id', $comment_balas_id)->where('user_id', $user_id)->delete();
            $message = 'Unlike success';
        } else {
            $comment->like = $comment->like + 1;
            $new_like_comment_balas = new LikeCommentBalas;
            $new_like_comment_balas->artikels_comment_balas_id = $comment_balas_id;
            $new_like_comment_balas->user_id = $user_id;
            $new_like_comment_balas->save();
            $message = 'Like success';
        }

        $comment->setTapActivity(['description' => 'Successfully to Like comment balas']); //kirim activitylog desc
        $comment->save();
        $commentUpdated = ArtikelsComment::with(array('likeComment' => function($query) use ($user_id) {
            $query->where('user_id', $user_id);
            }, 'user' => function($query) {
            $query->select('id', 'username', 'photo_profile');
            }, 'balas.user' => function($query) {
                $query->select('id', 'username', 'photo_profile');
            }, 'balas.likeCommentBalas' => function($query) use ($user_id) {
                $query->where('user_id', $user_id);
            }))
        ->find($comment->comment_id);


        return response(['message' => $message, 'comment' => $commentUpdated], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LikeCommentBalas  $likeCommentBalas
     * @return \Illuminate\Http\Response
     */
    public function show(LikeCommentBalas $likeCommentBalas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LikeCommentBalas  $likeCommentBalas
     * @return \Illuminate\Http\Response
     */
    public function edit(LikeCommentBalas $likeCommentBalas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LikeCommentBalas  $likeCommentBalas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LikeCommentBalas $likeCommentBalas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LikeCommentBalas  $likeCommentBalas
     * @return \Illuminate\Http\Response
     */
    public function destroy(LikeCommentBalas $likeCommentBalas)
    {
        //
    }
}
