<?php

namespace App\Http\Controllers\Api\Artikel;

use App\Http\Controllers\Controller;
use App\Models\LikeComment;
use App\Models\ArtikelsComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->id;
        $comment_id = $request->id;
        $comment = ArtikelsComment::find($comment_id);

        $like_comment = LikeComment::where('artikels_comment_id', $comment_id)
        ->where('user_id', $user_id)
        ->first();

        if ($like_comment) {
            $comment->like = $comment->like == 0 ? 0 : $comment->like - 1;
            LikeComment::where('artikels_comment_id', $comment_id)->where('user_id', $user_id)->delete();
            $message = 'Unlike success';
        } else {
            $comment->like = $comment->like + 1;
            $new_like_comment = new LikeComment;
            $new_like_comment->artikels_comment_id = $comment_id;
            $new_like_comment->user_id = $user_id;
            $new_like_comment->save();
            $message = 'Like success';
        }
        
        $comment->setTapActivity(['description' => 'Successfully to Like comment']); //kirim activitylog desc
        $comment->save();
        $comment->load(array('likeComment' => function($query) use ($user_id) {
            $query->where('user_id', $user_id);
            }, 'user' => function($query) {
            $query->select('id', 'username', 'photo_profile');
            }, 'balas.user' => function($query) {
            $query->select('id', 'username', 'photo_profile');
            }, 'balas.likeCommentBalas' => function($query) use ($user_id){
                $query->where('user_id', $user_id);
            }));

        return response(compact('message', 'comment'), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LikeComment  $likeComment
     * @return \Illuminate\Http\Response
     */
    public function show(LikeComment $likeComment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LikeComment  $likeComment
     * @return \Illuminate\Http\Response
     */
    public function edit(LikeComment $likeComment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LikeComment  $likeComment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LikeComment $likeComment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LikeComment  $likeComment
     * @return \Illuminate\Http\Response
     */
    public function destroy(LikeComment $likeComment)
    {
        //
    }
}
