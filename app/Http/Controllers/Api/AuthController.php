<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UsersEmailVerifiedRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Spatie\Activitylog\Models\Activity;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    private function _generateToken($user_id, $email)
    {
        $verified_token = Hash::make(Carbon::now()->timestamp . rand(100000,999999));

        $data_verified_req = [
            'user_id' => $user_id,
            'email' => $email,
            'token' => $verified_token
        ];
        $email_verified_token = new UsersEmailVerifiedRequest($data_verified_req);
        $email_verified_token->setTapActivity(['description' => 'Successfully to Generate verifikasi']); //kirim activitylog desc
        $email_verified_token->save();
    }

    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string',
            'username' => 'required|string|unique:users',
            'phone' => 'required|string',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x]).*$/',
            'password_confirmation' => 'required',
            'agreement' => 'required|boolean'
        ]);
        

        $validatedData['password'] = Hash::make($request->password);

        unset($validatedData['passwod_confirmation']);
        unset($validatedData['agreement']);
        $user = new User($validatedData);
        $user->setTapActivity(['description' => 'Successfully to register, User: ' . $request->username]); //kirim activitylog desc
        $user->save();
        $this->_generateToken($user->id, $user->email);

        return response(['message' => 'Akunmu berhasil didaftarkan, silahkan cek email Kamu untuk lakukan verifikasi'], 200);
    }

    public function login(Request $request)
    {
        $request->validate([
            'akun' => 'required|string',
            'password' => 'required|string',
        ]);

        $akun = User::where('username', $request->akun)->first();
        if ($akun) {
            $email = $akun->email;
        } else {
            $akun = User::where('email', $request->akun)->first();
            if (!$akun) {
                activity()
                    ->performedOn(new User)
                    ->tap(function(Activity $activity) {
                        $activity->ip_address = $_SERVER["REMOTE_ADDR"];
                    })->log(Auth::user()->username . 'Failed to Login, Username atau password salah');
                return response(['message' => 'Username atau password Anda salah'], 400);return response(['message' => 'Username atau password Anda salah'], 402);
            }
            $email = $request->akun;
        }
        $credentials = [
            'email' => $email,
            'password' => $request->password
        ];

        if (!Auth::attempt($credentials)) {
            activity()
                ->performedOn(new User)
                ->tap(function(Activity $activity) {
                    $activity->ip_address = $_SERVER["REMOTE_ADDR"];
                })->log($request->akun . ', Failed to Login, Username atau password salah');
            return response(['message' => 'Username atau password Anda salah'], 400);
        }
        $user = Auth::user();
        if ($user->email_verified_at == NULL) {
            activity()
            ->performedOn(new User)
            ->tap(function(Activity $activity) {
                $activity->ip_address = $_SERVER["REMOTE_ADDR"];
            })->log(Auth::user()['username'] . 'Failed to Login, not verified');
            return response(['message' => 'Silahkan cek email Kamu untuk lakukan verifikasi terlebih dahulu'], 401);
        }

        $accessToken = $user->createToken('authToken')->accessToken;

        activity()
            ->performedOn(new User)
            ->tap(function(Activity $activity) {
                $activity->ip_address = $_SERVER["REMOTE_ADDR"];
            })->log(Auth::user()->username . 'Successfull to Login');

        return response([
            'message' => 'Kamu berhasil login',
            'user' => Auth::user(),
            'access_token' => $accessToken,
            'token_type' => 'Bearer'
        ], 200);
    }

    public function sendEmailVerifiedRequest()
    {
        $verified_req_list = UsersEmailVerifiedRequest::with('user')->whereNull('send_req')->get();
        foreach ($verified_req_list as $verified_req) {
            $token = Crypt::encryptString($verified_req->token);
            $verified_req->verified_token = base64_encode($verified_req->id . '-' . $verified_req->email . '-' . $token);
            $verified_req->url = 'https://cakrawalabahasa.com';
            Mail::to($verified_req->email)->send(new \App\Mail\SendEmailVerifiedRequest($verified_req));

            UsersEmailVerifiedRequest::where('id', $verified_req->id)->update(['send_req' => 1]);
        }

        return response()->json(['message' => 'Notifikasi Pesan Anda berhasil terkirim'], 200);
    }

    public function verifikasiAkun($token)
    {
        $token_decode = base64_decode($token);
        $token_explode = explode("-", $token_decode);
        $id = $token_explode[0];
        $email = $token_explode[1];
        $token = Crypt::decryptString($token_explode[2]);

        $check_verified_req = UsersEmailVerifiedRequest::with('user')
            ->where('id', $id)
            ->where('email', $email)
            ->where('token', $token)
            ->orderBy('id', 'desc')
            ->first();
        
        if (empty($check_verified_req)) {
            activity()
                ->performedOn(new User)
                ->tap(function(Activity $activity) {
                    $activity->ip_address = $_SERVER["REMOTE_ADDR"];
                })->log(Auth::user()->username . 'Failed to verify');
            return response()->json(['message' => 'Token tidak valid, silahkan lakukan request email verifikasi kembali'], 422);
        } else if (Carbon::create($check_verified_req->created_at)->diffInHours(Carbon::now())) {
            $check_verified_req->setTapActivity(['description' => 'Token verify expired']); //kirim activitylog desc
            $check_verified_req->delete();
            return response()->json(['message' => 'Token tidak valid, silahkan lakukan request email verifikasi kembali'], 422);
        }

        if ($check_verified_req->user->email_verified_at != NULL)
            return response()->json(['message' => 'Akun kamu sudah terverifikasi sebelumnya, kamu bisa langsung saja signin ke akun kamu'], 422);
        
        $user = User::find($check_verified_req->user_id);
        $user->email_verified_at = Carbon::now();
        $user->setTapActivity(['description' => 'Successfully to verifikasi']); //kirim activitylog desc
        $user->save();
        $check_verified_req->delete();

        return response()->json(['message' => 'Email Anda telah terverifikasi, silahkan lakukan signin kembali'], 200);
    }

    public function kirimEmailVerifikasiAkun(Request $request)
    {
        $request->validate([
            'email' => 'email|required',
        ]);
        
        $user = User::where('email', $request->email)->first();
        if (empty($user)) {
            activity()
                ->performedOn(new User)
                ->tap(function(Activity $activity) {
                    $activity->ip_address = $_SERVER["REMOTE_ADDR"];
                })->log(Auth::user()->username . 'Email tidak terdaftar');
            return response()->json(['message' => 'Email tidak terdaftar'], 422);
        }
        if ($user->email_verified_at != NULL) {
            activity()
                ->performedOn(new User)
                ->tap(function(Activity $activity) {
                    $activity->ip_address = $_SERVER["REMOTE_ADDR"];
                })->log(Auth::user()->username . 'Akun sudah terverifikasi');
            return response()->json(['message' => 'Akun kamu sudah terverifikasi'], 422);
        }

        $this->_generateToken($user->id, $user->email);

        return response()->json(['message' => 'Silahkan cek email Kamu untuk lakukan verifikasi'], 200);
    }
}
