<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UsersEmaiForgotPass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Spatie\Activitylog\Models\Activity;

class ForgotPassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UsersEmaiForgotPass  $usersEmaiForgotPass
     * @return \Illuminate\Http\Response
     */
    public function show(UsersEmaiForgotPass $usersEmaiForgotPass)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UsersEmaiForgotPass  $usersEmaiForgotPass
     * @return \Illuminate\Http\Response
     */
    public function edit(UsersEmaiForgotPass $usersEmaiForgotPass)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UsersEmaiForgotPass  $usersEmaiForgotPass
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsersEmaiForgotPass $usersEmaiForgotPass)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UsersEmaiForgotPass  $usersEmaiForgotPass
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsersEmaiForgotPass $usersEmaiForgotPass)
    {
        //
    }

    private function _generateToken($user_id, $email)
    {
        $verified_token = Hash::make(Carbon::now()->timestamp . rand(100000,999999));

        $data_verified_req = [
            'user_id' => $user_id,
            'email' => $email,
            'token' => $verified_token
        ];
        $email_verified_token = new UsersEmaiForgotPass($data_verified_req);
        $email_verified_token->setTapActivity(['description' => 'Successfully to Generate Forgot Pass']); //kirim activitylog desc
        $email_verified_token->save();
    }

    public function checkToken(Request $request)
    {
        $request->validate([
            'token' => 'required',
        ]);

        $token_decode = base64_decode($request->token);
        $token_explode = explode("-", $token_decode);
        $id = $token_explode[0];
        $email = $token_explode[1];
        $token = Crypt::decryptString($token_explode[2]);

        $check_resetpass_req = UsersEmaiForgotPass::with('user')
            ->where('id', $id)
            ->where('email', $email)
            ->where('token', $token)
            ->orderBy('id', 'desc')
            ->first();
        
        if (empty($check_resetpass_req) || Carbon::create($check_resetpass_req->created_at)->diffInDays(Carbon::now())) {
            activity()
                ->performedOn(new User)
                ->tap(function(Activity $activity) {
                    $activity->ip_address = $_SERVER["REMOTE_ADDR"];
                })->log($email . 'Token Forgot pass not valid');
            return response()->json(['message' => 'Token tidak valid'], 422);
        }

        return response()->json(['message' => 'Token valid, silahan masukkan password baru'], 200);
    }

    public function ResetPass(Request $request)
    {
        $request->validate([
            'token' => 'required',
            'password' => 'required|confirmed|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x]).*$/',
            'password_confirmation' => 'required',
        ]);

        $token_decode = base64_decode($request->token);
        $token_explode = explode("-", $token_decode);
        $id = $token_explode[0];
        $email = $token_explode[1];
        $token = Crypt::decryptString($token_explode[2]);

        $check_resetpass_req = UsersEmaiForgotPass::with('user')
            ->where('id', $id)
            ->where('email', $email)
            ->where('token', $token)
            ->orderBy('id', 'desc')
            ->first();
        
        if (empty($check_resetpass_req)) {
            activity()
                ->performedOn(new User)
                ->tap(function(Activity $activity) {
                    $activity->ip_address = $_SERVER["REMOTE_ADDR"];
                })->log($email . 'Token tidak valid');
            return response()->json(['message' => 'Token tidak valid'], 422);
        } else if (Carbon::create($check_resetpass_req->created_at)->diffInDays(Carbon::now())) {
            $check_resetpass_req->setTapActivity(['description' => 'Token tidak valid']); //kirim activitylog desc
            $check_resetpass_req->delete();
            return response()->json(['message' => 'Token tidak valid'], 422);
        }

        $check_resetpass_req->delete();
        $user = User::where('email', $email)->first();
        $user->password = Hash::make($request->password);
        $user->setTapActivity(['description' => 'Successfully to Reset Pass']); //kirim activitylog desc
        $user->update();

        return response()->json(['message' => 'Reset Password berhasil'], 200);
    }

    public function kirimEmailForgotPass(Request $request)
    {
        $request->validate([
            'email' => 'email|required',
        ]);
        
        $user = User::where('email', $request->email)->first();
        if (empty($user)) {
            activity()
                ->performedOn(new User)
                ->tap(function(Activity $activity) {
                    $activity->ip_address = $_SERVER["REMOTE_ADDR"];
                })->log($request->email . 'Forgot Pass Email tidak terdaftar');
            return response()->json(['message' => 'Email tidak terdaftar'], 422);
        }

        $this->_generateToken($user->id, $user->email);

        return response()->json(['message' => 'Silahkan cek email Kamu untuk lakukan reset password'], 200);
    }
    
    public function sendEmailForgotPassRequest()
    {
        $forgotpass_req_list = UsersEmaiForgotPass::with('user')->whereNull('send_req')->get();
        foreach ($forgotpass_req_list as $forgotpass_req) {
            $token = Crypt::encryptString($forgotpass_req->token);
            $forgotpass_req->verified_token = base64_encode($forgotpass_req->id . '-' . $forgotpass_req->email . '-' . $token);
            $forgotpass_req->url = 'http://localhost:3000';
            Mail::to($forgotpass_req->email)->send(new \App\Mail\SendMailResetPasswordRequest($forgotpass_req));

            UsersEmaiForgotPass::where('id', $forgotpass_req->id)->update(['send_req' => 1]);
        }

        return response()->json(['message' => 'Email untuk reset password Anda berhasil terkirim'], 200);
    }
}
