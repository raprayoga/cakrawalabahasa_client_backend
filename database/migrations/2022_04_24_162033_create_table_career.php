<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCareer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career', function (Blueprint $table) {
            $table->id();
            $table->string('email', 255);
            $table->string('full_name', 255);
            $table->string('nick_name', 100);
            $table->string('gender', 10);
            $table->integer('age');
            $table->string('domicile', 255);
            $table->string('phone_number', 15);
            $table->string('institute', 50);
            $table->string('major', 50);
            $table->integer('semester');
            $table->string('intern_info', 50)->nullable();
            $table->string('position', 50);
            $table->string('why_match', 255)->nullable();
            $table->string('position2', 50)->nullable();
            $table->string('why_match2', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_career');
    }
}
