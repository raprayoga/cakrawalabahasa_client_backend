<!DOCTYPE html>
<html>

<head>
    <title>cakrawalabahasa.com</title>
</head>

<body>
    <h1>Notifikasi Pesan Masuk</h1>
    <p>Hallo kak {{ $details['username'] }}</p>
    <p>Aku adalah email otomatis system cakrawalabahasa.com</p>
    <p>Aku mau kasih informasi kalau ada {{ $details['count'] }} pesan masuk melalui form hubungi kami </p>
    <p>Segera periksa pesan masuk di dashboard admin https://admin.cakrawalabahasa.com/admin/login</p>

    <br>
    <p>Pesan ini akan dikirimkan lagi selama 2 jam kedepan jika pesan masuk tidak kunjung dibaca</p>

    <p>Thank you</p>
</body>

</html>