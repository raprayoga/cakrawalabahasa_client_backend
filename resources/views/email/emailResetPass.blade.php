<!DOCTYPE html>
<html>

<head>
    <title>cakrawalabahasa.com</title>
</head>

<style>
    body {
        margin: auto;
        max-width: 500px;
    }
</style>

<body>
    <h1>Halo, {{ $details->user->name }}</h1>
    <p>Selamat Datang di cakrawalabahasa.com</p>
    <p>Kami sudah menerima permintaan reset password. Klik tombol dibawah untuk mereset password akun kamu, atau salin link berikut:</p>
    <p>Jika kamu tidak merasa melakukan permintaan reset password, harap abaikan email ini</p>
    <div style="max-width: 500px; word-wrap: break-word;">
        <a href="{{ $details->url . '/reset-password/' . $details->verified_token }}" >
            {{ $details->url . '/reset-password/' . $details->verified_token }}
        </a>
    </div>
    <br>
    <a href="{{ $details->url. '/reset-password/' . $details->verified_token }}" style="text-decoration: none;">
        <div style="background-color: #f78a28;
        border-radius: 10px;
        padding: 1px;
        text-align: center;
        color: white;
        width: 50%;
        margin: 100px auto;">
          <p style="font-weight: bold;">Reset Password</p>
        </div>
    </a>

    <p>Thank you</p>
</body>

</html>