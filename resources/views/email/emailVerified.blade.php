<!DOCTYPE html>
<html>

<head>
    <title>cakrawalabahasa.com</title>
</head>

<style>
    body {
        margin: auto;
        max-width: 500px;
    }
</style>

<body>
    <h1>Halo, {{ $details->user->name }}</h1>
    <p>Selamat Datang di cakrawalabahasa.com</p>
    <p>Kami sudah menerima form registrasi kamu, sebelum memulai perjalanan lebih lanjut kamu harus melakukan verifikasi email akun kamu. Klik tombol dibawah untuk verifikasi akun email kamu, atau salin link berikut:</p>
    <div style="max-width: 500px; word-wrap: break-word;">
        <a href="{{ $details->url . '/verifikasi-akun/' . $details->verified_token }}" >
            {{ $details->url . '/verifikasi-akun/' . $details->verified_token }}
        </a>
    </div>
    <br>
    <a href="{{ $details->url. '/verifikasi-akun/' . $details->verified_token }}" style="text-decoration: none;">
        <div style="background-color: #f78a28;
        border-radius: 10px;
        padding: 1px;
        text-align: center;
        color: white;
        width: 50%;
        margin: 100px auto;">
          <p style="font-weight: bold;">Verifikasi Akun</p>
        </div>
    </a>

    <p>Thank you</p>
</body>

</html>