<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('sign-up', 'Api\AuthController@register');
Route::post('sign-in', 'Api\AuthController@login');

Route::resource('contact-us', 'Api\ContactUsController');
Route::resource('dokumentasi', 'Api\DokumentasiBatchController');
Route::post('dokumentasi/list', 'Api\DokumentasiBatchController@list');
Route::get('pageview', 'Api\HomeController@addPageView');

Route::get('sendnotifhubungisaya', 'Api\ContactUsController@sendNotifHubungiSaya');
Route::get('sendemailverifiedreq', 'Api\AuthController@sendEmailVerifiedRequest');
Route::get('verifikasi-akun/{token}', 'Api\AuthController@verifikasiAkun');
Route::post('kirim-email-verifikasi', 'Api\AuthController@kirimEmailVerifikasiAkun');

Route::get('sendemailforgotpass', 'Api\ForgotPassController@sendEmailForgotPassRequest');
Route::post('checktoken-resetpass/', 'Api\ForgotPassController@checkToken');
Route::post('reset-password/', 'Api\ForgotPassController@ResetPass');
Route::post('kirim-email-lupa-password', 'Api\ForgotPassController@kirimEmailForgotPass');

Route::post('career/apply/{position}', 'Api\CareerController@apply');

Route::group(['prefix' => 'regist'], function () {
    Route::resource('int-heroes', 'Api\Registration\RegistIntHeroesController');
    Route::resource('local-heroes', 'Api\Registration\RegistLocalHeroesController');
    Route::resource('member', 'Api\Registration\RegistMemberController');
});

Route::group(['prefix' => 'artikel'], function () {
    Route::resource('artikel-kategori', 'Api\Artikel\ArtikelKategoriController');
    Route::resource('artikel-featured', 'Api\Artikel\ArtikelFeaturedController');
    Route::resource('artikel', 'Api\Artikel\ArtikelController');
    Route::post('artikel-list', 'Api\Artikel\ArtikelController@list');
    Route::get('artikel-populer', 'Api\Artikel\ArtikelController@populer');
    
    Route::get('artikels-comment/{id}/with-artikel-id', 'Api\Artikel\ArtikelsCommentController@commentWithIdArtikel');
    
    Route::group(['middleware' => 'auth:api'], function () {
        Route::resource('artikels-comment', 'Api\Artikel\ArtikelsCommentController');
        Route::resource('balas-comment', 'Api\Artikel\ArtikelsCommentBalasController');
        Route::resource('like-comment', 'Api\Artikel\LikeCommentController');
        Route::resource('like-comment-balas', 'Api\Artikel\LikeCommentBalasController');
        Route::get('artikels-comment/{id}/with-artikel-id/login', 'Api\Artikel\ArtikelsCommentController@commentWithIdArtikelLogin');
    });
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::resource('profile', 'Api\ProfileController');
    Route::post('profile/change-password', 'Api\ProfileController@changePassword');
});

Route::group(['prefix' => 'report'], function () {
    Route::group(['middleware' => 'auth:api'], function () {
        Route::resource('report-comment', 'Api\Report\ReportCommentsController');
        Route::resource('report-balas-comment', 'Api\Report\ReportCommentBalasController');
    });
});